import Graphics.UI.Gtk

main :: IO ()
main =
    do initGUI
       win <- windowNew
       windowSetTitle win "Adamov's Example"
       win `onDestroy` mainQuit

       ent <- entryNew
       btn <- buttonNew

       col <- vBoxNew False 5
       containerAdd col ent
       containerAdd col btn

       buttonSetLabel btn "Click to Print"
       entrySetText ent "Hello World"

       btn `onClicked` do s <- entryGetText ent
                          print s

       containerAdd win col
       widgetShowAll win
       mainGUI