import System.Random


double:: (Num a) => a -> a
double x = x + x

quadruple:: Integer -> Integer
quadruple = double.double

factorial n = product [1..n]

average ns = sum ns `div` length ns
--factorial :: (Integral a) => a -> a
--factorial 0 = 1
--factorial n = n * factorial(n - 1)

doubleThis :: (Integral x) => x -> x
doubleThis x = x + x

lucky :: (Integral a) => a -> String
lucky 7 = "Lucky number seven!"
lucky x = "Try again!"

sayMe :: (Integral a) => a -> String  
sayMe 1 = "One!"  
sayMe 2 = "Two!"
sayMe 3 = "Three!"  
sayMe 4 = "Four!"  
sayMe 5 = "Five!"  
sayMe x = "Not between 1 and 5"  

addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)

head' :: [a] -> a
head' [] = error "This is an empty list! Give me more!"
head' (x:_) = x

capital :: String -> String
capital "" = "Empty string; no capital!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height  
    | bmi <= 18.5 = "You're underweight, you emo, you!"  
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise   = "You're a whale, congratulations!"  
    where bmi = weight / height ^ 2  
    
maximum' :: (Ord a) => [a] -> a  
maximum' [] = error "maximum of empty list"  
maximum' [x] = x  
maximum' (x:xs) = max x (maximum' xs) 

minimum' :: (Ord a) => [a] -> a
minimum' [] = error "minimum of empty list"
minimum' [x] = x
minimum' (x:xs) = min x (minimum' xs)

replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' n x
    | n <= 0 = []
    | otherwise = x:replicate' (n-1) x

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' n _
    | n <= 0 = []
take' _ [] = []
take' n (x:xs) = x : take' (n-1) xs

reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

zip' :: [a] -> [b] -> [(a, b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x, y):zip' xs ys

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let smallerSorted = quicksort (filter (<= x) xs)
        biggerSorted = quicksort (filter (>x) xs)
    in smallerSorted ++ [x] ++ biggerSorted
    
    
multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred = compare 100 

-- Infix demonstration. Side missing a number, when you pass it in, will put itself IN that space.
divideByTen :: (Floating a) => a -> a
divideByTen = (/10)

applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]  
zipWith' _ [] _ = []  
zipWith' _ _ [] = []  
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [100000, 99999..])
    where p x = x `mod` 3829 == 0
    
-- Find the closest number to the passed in number that is divisible by
-- the second parameter. X is the number to begine at, i.e. 1000, and Y
-- is the multiple you are looking for, i.e. 64. Returns 1024, the next
-- number closest to 1000 that is divisible by 64.
--Number x is the number to begin at (i.e. 1000) while y is the factor (i.e. 64). 
--Function looks for next number greater than x that is mod 0 by the factor,
--in this case 1024.
closestFactor :: (Num a, Integral a) => a -> a -> a
closestFactor _ 0 = error "Dividing by 0"
closestFactor x y
    |x `mod` y /= 0 = closestFactor (x + 1) y
    |x `mod` y == 0  = x
    
--multiplyFactor :: (Num a) => a -> a -> a
--multiplyFactor x y = x + (64 * y)
    
    
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
    | even n = n:chain (n `div` 2)
    | odd n = n:chain (n * 3 + 1)

numLongChains :: Int 
numLongChains = length (filter isLong (map chain [1..100]))
    where isLong xs = length xs > 15
    
sum' :: (Num a) => [a] -> a
sum' = foldl (+) 0

elem' :: (Eq a) => a -> [a] -> Bool
elem' y ys = foldl (\acc x -> if x == y then True else acc) False ys

--last' :: [a] -> a
last' [] = error "No last of an empty list!"
last' ns = head (drop (length ns - 1) ns)   

-- Random password generator. First pass through creates a newgen as a paramter. 
-- When the second one is calculated, random is called on that new gen and creates
-- another one for the third one. Finally, that one uses the gen of the second one,
-- which is bound to be different from the first.
threeCoins :: StdGen -> (Bool, Bool, Bool)
threeCoins gen = 
    let (firstCoin, newGen) = random gen
        (secondCoin, newGen') = random newGen
        (thirdCoin, newGen'') = random newGen'
    in (firstCoin, secondCoin, thirdCoin)

myLast :: [a] -> a
myLast [] = error "Empty list!"
myLast ns = head $ drop (length ns - 1) ns

myButLast :: [a] -> [a]
myButLast [] = error "Empty list!"
myButLast ns = take (length ns - 1) ns

elementAt [] x = error "Empty list!"
elementAt xs x = head (drop ((length (take x xs)) - 1) xs)


