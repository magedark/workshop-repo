-- Finding last element of list
myLast :: [a] -> a
myLast [] = error "Empty list"
myLast [x] = x
myLast (x:xs) = myLast xs


-- Finding second to last element
myButLast :: [a] -> a
myButLast [] = error "Empty list"
myButLast [x] = error "Only one element"
myButLast (x:xs) = myButLast xs
--myButLast (x:xs) = x
